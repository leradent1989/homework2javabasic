package homework2;

import java.util.Random;
import java.util.Scanner;

public  class Main {

    public static void main(String[] args) {
        String[][] board = new String[5][5];
        Random random = new Random();
        int neededLine = random.nextInt(5);
        int neededColumn = random.nextInt(5);
        while (neededLine == 0 || neededColumn == 0) {
            neededLine = random.nextInt(5);
            neededColumn = random.nextInt(5);
        }
        System.out.println("All set. Get ready to rumble!");
        boolean bool = true;
      while(bool)  {
            Scanner in = new Scanner(System.in);

            System.out.println("Enter line");
            String lineStr = in.nextLine();
            while (!isInteger(lineStr) || Integer.parseInt(lineStr) < 1 || Integer.parseInt(lineStr) > 5 ){
             if(!isInteger(lineStr) ) { System.out.println(" Not a number Enter number ");}
             else if(Integer.parseInt(lineStr) < 1 || Integer.parseInt(lineStr) > 5 ){System.out.println("Enter number more then 1 and less then 5");}
                lineStr = in.nextLine();
            }
            System.out.println("Enter column");
            String columnStr = in.nextLine();
            while (!isInteger(columnStr) || Integer.parseInt(columnStr) < 1 || Integer.parseInt(columnStr) > 5){
              if(!isInteger(columnStr)) { System.out.println("Not a number Please enter number ");}
              else if(Integer.parseInt(columnStr) < 1 || Integer.parseInt(columnStr) > 5){System.out.println("Enter number more then 1 and less then 5");}
                columnStr = in.nextLine();
            }
            int line = Integer.parseInt(lineStr);
            int column = Integer.parseInt(columnStr);

            if (line == neededLine && neededColumn == column) {
                bool = false;
                System.out.println("Congratulations You have won!");
            }
            printGameField(board,line,column,neededLine,neededColumn);
        }
    }
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }
    public static void printGameField(String [] [] board,int line,int column,int neededLine,int neededColumn) {
        for (int i = 0; i < board.length ; i++) {
            for (int j = 0; j < board[0].length ; j++) {
                if (line == neededLine && column == neededColumn) {
                    int c = line -1;
                    int b =column -1;
                    board[c][b] = "x";

                }  else if (i == (line -1) && j == (column -1) || board[i][j] == "*") {

                    board[i][j] = "*";
                } else {
                    board[i][j] = "-";
                }
            }
        }
        System.out.print("0|");
        for (int i = 0; i < board.length ; i++) {
            if (i == 0) {
                for (int j = 0; j < board[0].length ; j++) {
                    if(j== board[0].length -1){
                        System.out.print((j + 1) + "|\n");
                    }
                    else System.out.print((j + 1) + "|");
                }
            }
            for (int j = 0; j < board[0].length; j++) {
                if(j == 0){
                    System.out.print( (i + 1) + "|");
                }
                System.out.print("" + board[i][j] + "|");
            }

                 System.out.println("");
        }
    }
}
